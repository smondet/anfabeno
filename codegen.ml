open! Base
open! Stdio

let santize_ocaml = String.map ~f:(function '-' -> '_' | c -> c)

let module_name_of_row row =
  List.hd_exn row |> fst |> santize_ocaml |> String.capitalize

let module_of_row ppf row =
  let module_name = module_name_of_row row in
  let object_type ppf row =
    let object_field ppf (n, k) : unit =
      Fmt.pf ppf "%s:@ %s" (santize_ocaml n)
        (match k with `Kind -> "unit" | `String -> "string" | `Int -> "int")
    in
    Fmt.pf ppf "%a" Fmt.(box (list ~sep:(any ";@ ") object_field)) row in
  let of_row ppf row =
    let pattern ppf row =
      Fmt.pf ppf "[@[@ %a@ @]]"
        Fmt.(list ~sep:(any ";@ ") string)
        (List.map row ~f:(fun (n, _) -> santize_ocaml n)) in
    let object_methods ppf row =
      List.iter row ~f:(fun (n, k) ->
          Fmt.pf ppf "method %s = %s@." (santize_ocaml n)
            ( match k with
            | `Kind -> Fmt.str "assert String.(%s = %S); ()" (santize_ocaml n) n
            | `String -> santize_ocaml n
            | `Int ->
                Fmt.str "(try Int.of_string %s with _ -> Fmt.failwith %S %s)"
                  (santize_ocaml n) "Cannot parse integer from %S"
                  (santize_ocaml n) )) in
    Fmt.pf ppf
      "fun row -> match row with %a -> object %a end\n\
       | other -> Fmt.failwith \"Cannot parse %s.t from [%%a]\" Fmt.Dump.(list \
       string) other"
      pattern row object_methods row module_name in
  let inside_module ppf row =
    Fmt.pf ppf "let fields = [%a]@ type t = <%a>@ let of_row =@ @[<2>%a@]"
      Fmt.(list ~sep:(any ",@ ") (using fst (quote string)))
      row object_type row of_row row in
  Fmt.pf ppf "module %s = struct@ @[<2>%a@]@ end@ " module_name inside_module
    row

let () =
  let input = Caml.Sys.argv.(1) in
  let cells =
    In_channel.read_lines input
    |> List.map ~f:(fun line ->
           String.split ~on:'\t' line
           |> List.map ~f:(fun cell ->
                  match String.split ~on:':' cell with
                  | [name; "string"] -> (name, `String)
                  | [name; "int"] -> (name, `Int)
                  | [name; "kind"] -> (name, `Kind)
                  | _ ->
                      Fmt.failwith "Wrong cell %S in line [%a]" cell
                        Fmt.Dump.(string)
                        line)) in
  let header =
    Fmt.str {ocaml|(** Generated code from [%S] *)
open! Base
|ocaml} input
  in
  let buffer = Buffer.create 1000 in
  Buffer.add_string buffer header ;
  let ppf = Fmt.with_buffer ~like:Fmt.stdout buffer in
  Fmt.pf ppf "let all_names = [" ;
  List.iter cells ~f:(fun row -> Fmt.pf ppf "%S;" (List.hd_exn row |> fst)) ;
  Fmt.pf ppf "]" ;
  List.iter cells ~f:(module_of_row ppf) ;
  Fmt.pf ppf "@.type t = %a"
    Fmt.(
      let variant_case ppf row =
        Fmt.pf ppf "%s of %s.t" (module_name_of_row row)
          (module_name_of_row row) in
      list ~sep:(any "|@ ") variant_case)
    cells ;
  let parsing_cases ppf cells =
    List.iter cells ~f:(fun row ->
        Fmt.pf ppf "| %S -> %s (%s.of_row row)@."
          (List.hd_exn row |> fst)
          (module_name_of_row row) (module_name_of_row row)) in
  Fmt.pf ppf
    "@.let of_rows rows = List.map rows ~f:(fun row -> match List.hd_exn row\n\
     with@.%a| other -> Fmt.failwith %S other@.| exception _ -> Fmt.failwith \
     %S)@."
    parsing_cases cells "Cannot recognize row header: %S" "Empty row" ;
  Fmt.flush ppf () ;
  let data = Buffer.contents buffer in
  try Out_channel.write_all ~data Caml.Sys.argv.(2)
  with _ -> Fmt.pr "```ocaml\n%s\n```\n%!" data
