open! Base
open! Stdio

type common =
  < address: string
  ; balance_before: int
  ; balance_after: int (* ; consumed_gas : int *)
  ; flavor: string >

let to_common =
  let open Result_types in
  function
  | Transfer x -> Some (x :> common)
  | Mint x -> Some (x :> common)
  | Origination x -> Some (x :> common)
  | Add_operators x -> Some (x :> common)
  | Failed_transfer _ -> None

let cmd s =
  match Caml.Sys.command s with
  | 0 -> ()
  | other -> Fmt.failwith "Command %S failed with %d" s other

let make_plot ~y_axis ?(labels = []) plots ~title ~output_png =
  let script = Caml.Filename.chop_extension output_png ^ ".gnuplot" in
  let window = snd y_axis in
  let font = "Helvetica,22" in
  let title_font = "Helvetica Bold,28" in
  Fmt.kstr cmd "mkdir -p %s" Caml.Filename.(dirname output_png |> quote) ;
  Out_channel.write_lines script
    ( [ Fmt.str "print 'MAKING %S GNUPLOT'" output_png
      ; Fmt.str "set output '%s'" output_png
      ; Fmt.str
          "set term pngcairo noenhanced size 1800,1000 linewidth 4 rounded  \
           font %S"
          font; Fmt.str "set title '%s' font '%s'" title title_font
      ; "set xtics auto"; "set format y \"%.4g\"" ]
    @ List.map labels ~f:(fun (d, l) ->
          Fmt.str
            "set label '%s' at %d,%d rotate right textcolor rgb '#00005500' \
             point lt 1 pt 2 offset 2,6"
            (* offset 1,30 *) l d window)
    @ [Fmt.str "set yrange [%d:%d]" (fst y_axis) (snd y_axis)]
    @ [ Fmt.str "plot %s"
          ( List.map plots ~f:(fun (`Data_file (data_file, title)) ->
                Fmt.str "'%s' using 1:2 with lines title '%s' " data_file title)
          |> String.concat ~sep:", \\\n" ) ]
    @ ["quit"] ) ;
  Fmt.kstr cmd "gnuplot -c %s" script ;
  ()

let make_tsv l =
  let out = Caml.Filename.temp_file "anfabeno" ".tsv" in
  let min_y = ref Int.max_value in
  let max_y = ref Int.min_value in
  Out_channel.with_file out ~f:(fun o ->
      List.iter l ~f:(fun (i, j) ->
          min_y := min !min_y j ;
          max_y := max !max_y j ;
          Out_channel.fprintf o "%d\t%d\n" i j) ;
      ()) ;
  (out, !min_y, !max_y)

let lifetime_tez_usage_file (l : common list) =
  let data =
    let res = ref [] in
    let (_ : int) =
      List.foldi l ~init:0 ~f:(fun idx sum current ->
          let cost = current#balance_before - current#balance_after in
          let accumulated = sum + cost in
          res := (idx, accumulated) :: !res ;
          accumulated) in
    List.rev !res in
  make_tsv data

let find_all_addresses l =
  List.map l ~f:(fun c -> c#address)
  |> List.dedup_and_sort ~compare:String.compare

let title_of_transfer one_transfer =
  Fmt.str "%s#%s (batch: %d (%s), ops: %d, toks: %d)" one_transfer#flavor
    (String.subo one_transfer#address ~len:8)
    one_transfer#batch_size one_transfer#batching_style
    one_transfer#operators_nb one_transfer#tokens_nb

let title_of_mint one_mint =
  Fmt.str "%s#%s" one_mint#flavor (String.subo one_mint#address ~len:8)

let lifetime_tez_usage rows ~output =
  let commons = List.filter_map ~f:to_common rows in
  let all_addresses = find_all_addresses commons in
  let min_cost = ref Int.max_value in
  let max_cost = ref Int.min_value in
  let plots =
    List.map all_addresses ~f:(fun addr ->
        let subrows =
          List.filter commons ~f:(fun r -> String.equal r#address addr) in
        let data, mi, ma = lifetime_tez_usage_file subrows in
        min_cost := min !min_cost mi ;
        max_cost := max !max_cost ma ;
        let one_transfer =
          Option.value_exn ~message:"lifetime_tez_usage: finding a transfer"
            (List.find_map rows ~f:(function
              | Transfer t when String.equal t#address addr -> Some t
              | _ -> None)) in
        let title = title_of_transfer one_transfer in
        `Data_file (data, title)) in
  let y_axis =
    let margin = (!max_cost - !min_cost) / 10 in
    (!min_cost - margin, !max_cost + (3 * margin)) in
  let output_png = Fmt.str "%s/lifetime-usage.png" output in
  let title = "Accumulated usage price (μꜩ)" in
  make_plot plots ~y_axis ~output_png ~title ;
  output_png

let transfer_gas_usage rows ~output =
  let all_transfers =
    List.filter_map rows
      ~f:Result_types.(function Transfer t -> Some t | _ -> None) in
  let all_addresses = find_all_addresses all_transfers in
  let min_gas = ref Int.max_value in
  let max_gas = ref Int.min_value in
  let plots =
    List.map all_addresses ~f:(fun addr ->
        let transfers =
          List.filter all_transfers ~f:(fun r -> String.equal r#address addr)
        in
        let to_plot =
          List.map transfers ~f:(fun tr -> (tr#ith, tr#consumed_gas)) in
        let tsv, miy, may = make_tsv to_plot in
        min_gas := min miy !min_gas ;
        max_gas := max may !max_gas ;
        let title = title_of_transfer (List.hd_exn transfers) in
        `Data_file (tsv, title)) in
  let y_axis = (0, 1_040_000) in
  let output_png = Fmt.str "%s/transfer-gas-usage.png" output in
  let title =
    Fmt.str "Gas usage per transfer (min: %d, max: %d)" !min_gas !max_gas in
  make_plot plots ~y_axis ~output_png ~title ;
  output_png

let batch_sizes_gas rows ~output =
  let all_transfers =
    List.filter_map rows
      ~f:Result_types.(function Transfer t -> Some t | _ -> None) in
  let all_flavors =
    List.map all_transfers ~f:(fun t -> t#flavor)
    |> List.dedup_and_sort ~compare:String.compare in
  let min_gas = ref Int.max_value in
  let max_gas = ref Int.min_value in
  let plots =
    List.map all_flavors ~f:(fun flav ->
        let transfers =
          List.filter all_transfers ~f:(fun r -> String.equal r#flavor flav)
        in
        let to_plot =
          List.map transfers ~f:(fun tr -> (tr#batch_size, tr#consumed_gas))
        in
        let tsv, miy, may = make_tsv to_plot in
        min_gas := min miy !min_gas ;
        max_gas := max may !max_gas ;
        let title = title_of_transfer (List.hd_exn transfers) in
        `Data_file (tsv, title)) in
  let y_axis = (0, 1_040_000) in
  let output_png = Fmt.str "%s/transfer-gas-usage-vs-batch-size.png" output in
  let title =
    let failed_transfer =
      List.filter_map rows ~f:(function
        | Result_types.Failed_transfer f -> Some f#batch_size
        | _ -> None)
      |> List.dedup_and_sort ~compare:Int.compare
      |> function
      | [] -> "found no failures" | one :: _ -> Fmt.str "fails at %d" one in
    Fmt.str "Gas usage per transfer Vs batch-size (min: %d, max: %d, %s)"
      !min_gas !max_gas failed_transfer in
  make_plot plots ~y_axis ~output_png ~title ;
  output_png

let operators_gas rows ~output =
  let all_transfers =
    List.filter_map rows
      ~f:Result_types.(function Transfer t -> Some t | _ -> None) in
  let all_flavors =
    List.map all_transfers ~f:(fun t -> t#flavor)
    |> List.dedup_and_sort ~compare:String.compare in
  let min_gas = ref Int.max_value in
  let max_gas = ref Int.min_value in
  let plots =
    List.map all_flavors ~f:(fun flav ->
        let transfers =
          List.filter all_transfers ~f:(fun r -> String.equal r#flavor flav)
        in
        let to_plot =
          List.map transfers ~f:(fun tr -> (tr#operators_nb, tr#consumed_gas))
        in
        let tsv, miy, may = make_tsv to_plot in
        min_gas := min miy !min_gas ;
        max_gas := max may !max_gas ;
        let title = title_of_transfer (List.hd_exn transfers) in
        `Data_file (tsv, title)) in
  let y_axis = (0, 1_040_000) in
  let output_png = Fmt.str "%s/transfer-gas-usage-vs-operators-nb.png" output in
  let title =
    let failed_transfer =
      List.filter_map rows ~f:(function
        | Result_types.Failed_transfer f -> Some f#batch_size
        | _ -> None)
      |> List.dedup_and_sort ~compare:Int.compare
      |> function
      | [] -> "found no failures" | one :: _ -> Fmt.str "fails at %d" one in
    Fmt.str
      "Gas usage per transfer Vs Number of Operators (min: %d, max: %d, %s)"
      !min_gas !max_gas failed_transfer in
  make_plot plots ~y_axis ~output_png ~title ;
  output_png

let mint_gas_usage rows ~output =
  let all_mints =
    List.filter_map rows
      ~f:Result_types.(function Mint t -> Some t | _ -> None) in
  let all_addresses = find_all_addresses all_mints in
  let min_gas = ref Int.max_value in
  let max_gas = ref Int.min_value in
  let plots =
    List.map all_addresses ~f:(fun addr ->
        let mints =
          List.filter all_mints ~f:(fun r -> String.equal r#address addr) in
        let to_plot =
          List.mapi mints ~f:(fun idx tr ->
              (* Fmt.epr "idx: %d, id: %d\n%!" idx tr#token_id ; *)
              assert (1 + idx = tr#token_id) ;
              (idx, tr#consumed_gas)) in
        let tsv, miy, may = make_tsv to_plot in
        min_gas := min miy !min_gas ;
        max_gas := max may !max_gas ;
        let title = title_of_mint (List.hd_exn mints) in
        `Data_file (tsv, title)) in
  let y_axis = (0, 1_040_000) in
  let output_png = Fmt.str "%s/mint-gas-usage.png" output in
  let title =
    Fmt.str "Gas usage per (NFT-style) mint (min: %d, max: %d)" !min_gas
      !max_gas in
  make_plot plots ~y_axis ~output_png ~title ;
  output_png

let mints_vs_big_nats_gas rows ~exponent_string ~output =
  let all_mints =
    List.filter_map rows
      ~f:Result_types.(function Mint t -> Some t | _ -> None) in
  let all_flavors =
    List.map all_mints ~f:(fun t -> t#flavor)
    |> List.dedup_and_sort ~compare:String.compare in
  let min_gas = ref Int.max_value in
  let max_gas = ref Int.min_value in
  let plots =
    List.map all_flavors ~f:(fun flav ->
        let mints =
          List.filter all_mints ~f:(fun r -> String.equal r#flavor flav) in
        let to_plot =
          List.map mints ~f:(fun tr -> (tr#token_id, tr#consumed_gas)) in
        let tsv, miy, may = make_tsv to_plot in
        min_gas := min miy !min_gas ;
        max_gas := max may !max_gas ;
        let title = title_of_mint (List.hd_exn mints) in
        `Data_file (tsv, title)) in
  let y_axis = (0, 1_040_000) in
  let output_png =
    Fmt.str "%s/mint-gas-usage-vs-amount-%s.png" output exponent_string in
  let title =
    Fmt.str "Gas usage per mint Vs amount: 2^(n * %s) (min: %d, max: %d)"
      exponent_string !min_gas !max_gas in
  make_plot plots ~y_axis ~output_png ~title ;
  output_png

let () =
  let action = Caml.Sys.argv.(1) in
  let input = Caml.Sys.argv.(2) in
  let output = Caml.Sys.argv.(3) in
  let tsv =
    In_channel.read_lines input
    |> List.map ~f:(fun line -> String.split ~on:'\t' line) in
  let rows = Result_types.of_rows tsv in
  let pngs =
    match action with
    | "transfers" ->
        let lifetime_tez_png = lifetime_tez_usage rows ~output in
        let gas_transfer_png = transfer_gas_usage rows ~output in
        [lifetime_tez_png; gas_transfer_png]
    | "batches" ->
        let gas_transfer_png = batch_sizes_gas rows ~output in
        [gas_transfer_png]
    | "operators" ->
        let gas_transfer_png = operators_gas rows ~output in
        [gas_transfer_png]
    | "bignats" ->
        let lifetime_tez_png =
          try [lifetime_tez_usage rows ~output] with _ -> [] in
        let gas_mint_png =
          mints_vs_big_nats_gas rows ~output ~exponent_string:Caml.Sys.argv.(4)
        in
        lifetime_tez_png @ [gas_mint_png]
    | "mints" ->
        let lifetime_tez_png =
          try [lifetime_tez_usage rows ~output] with _ -> [] in
        let gas_mint_png = mint_gas_usage rows ~output in
        lifetime_tez_png @ [gas_mint_png]
    | _ -> Fmt.failwith "Unkown action: %S" action in
  Fmt.pr "@[<2>%s:@ %d rows,@ %a@ [@ %a@ ]\n%!"
    (Caml.Filename.basename input)
    (List.length rows)
    Fmt.(list ~sep:(const string ", ") string)
    Result_types.all_names
    Fmt.(list ~sep:(any "@ ") string)
    pngs
