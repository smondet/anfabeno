#!/bin/sh

set -e

eval $(opam env)

make_report () {
    dune exec ./codegen.exe results-types.tsv

    rm -fr _out
    mkdir -p _out/
    fig () {
        printf "\n![]($PWD/$1){ width=99%% }\n\n" >> _out/results.md
    }
    cat > _out/results.md <<'EOF'
## Results

### 2000 Transfers

Compares multi Vs single, and SmartPy's normal Vs lazy-entry-points2 compilation.

Transfers are non-batched (i.e. batch-size = 1), and there is only one token.

We only see a couple of interesting things:

- Even when using one-single-token, the difference between using a
  custom build of FA2-Smartpy and the default is barely noticeable.
- The new "lazy-entry-points" option of smartpy is a bit more
  expensize at origination but then catches up by consuming less gas.

EOF
    fig _out/st2000/lifetime-usage.png
    fig _out/st2000/transfer-gas-usage.png
    mkdir -p _out/st2000/
    grep -E '(KT1HP81Vd1|KT1JrD5|KT1NbaHxT)' ./results-2000-signle-transfers.tsv \
         > _out/st2000/fact.tsv
    dune exec ./main.exe transfers _out/st2000/fact.tsv  _out/st2000/

    cat >> _out/results.md <<'EOF'
### Batch Sizes

`mutran_contract` is the default FA2-Smartpy with an extra entrypoint
to transfer `mutez`

We also compare with the `lazy-entry-points2` compilation.

We grow batch size (same “from”): 1, 11, 21, 31 → until we reach the
max-operation-size failure.

We see that the gas-usage grows with batch-size but does not reach
“dangerous” levels (at about 55%) before hitting the operation-size
limit.

EOF
    fig _out/b3/transfer-gas-usage-vs-batch-size.png
    dune exec ./main.exe batches results-b3-batching.tsv _out/b3/


    cat >> _out/results.md <<'EOF'
### Operators

Same builds as for the batch-sizes

We grow the number of operators added for the account doing the transfers:
1, 50, 100, ... 1000 (option `--operators "$(seq -s , 1 50 1000)"`)

This ran for 33 minutes.

EOF
    fig _out/b4/transfer-gas-usage-vs-operators-nb.png
    dune exec ./main.exe operators results-b4-ops.tsv _out/b4/

    cat >> _out/results.md <<'EOF'
### 5000 Transfers: Trying Debug and No-layouts

- More multi-token flavors: 
    - `contract`: The default.
    - `dbg_contract`: Debug mode = use maps instead of big-maps everywhere.
    - `nolay_contract`: do not apply the “right-comb” layouts required by the TZIP-12 specification (= let smartpy choose a more optimal layout)
    - `lzep2_contract`: use SmartPy's new compilation option for lazy entrypoints
- 0 operators, 50000 single-transfers
- ran for 4 hours

We see that:

- `dbg_contract` fails by hitting the gas-limit after a few 100 transfers
  (expected since all the values in a Michelson `map` consume gas to
  be deserialized)
- `nolay_contract`: Right-comb layouts (139939 gas units) Vs binary
  trees (139849 gas units) make no visible difference here
- lazy-entryoints still “scale” slightly better

EOF
    fig _out/b5/lifetime-usage.png
    fig _out/b5/transfer-gas-usage.png
    # B5 ran for about 4h
    dune exec ./main.exe transfers results-b5-dbg-nolay.tsv _out/b5/

    cat >> _out/results.md <<'EOF'
### 2000 Transfers: Bigger Batches

- flavors: `mutran_contract` `lzep2_mutran_contract` `single_mutran_contract`
- batch-size: 100
- 10 operators
- ran for 9 hours

With 100 exchanges per batch we see that the gains in gas are not noticeable
in total mutez usage (storage burn becomes the most important part).

EOF
    fig _out/b2/lifetime-usage.png
    fig _out/b2/transfer-gas-usage.png
    # This B2 ran for 9h
    dune exec ./main.exe transfers results-b2-bigger-batches.tsv _out/b2/


    cat >> _out/results.md <<'EOF'
### 10 000 Transfers: Extra-entrypoint Effect

- flavors: `mutran_contract` Vs `contract`
- batch-size: 10
- 0 operators
- ran for 6 hours and 30 minutes

⇒ The extra-entry point of the `mutran_contract` build, is negligible here.

EOF
    fig _out/b7/lifetime-usage.png
    fig _out/b7/transfer-gas-usage.png
    # This B7 ran for 6h30.
    dune exec ./main.exe transfers resulst_b7_mutran_vs_default_like_b6.tsv _out/b7/

    cat >> _out/results.md <<'EOF'
### 10 000 Transfers: No-Operators + Permissions Descriptor

- flavors: `contract` Vs `perdesc_noops_contract`
- batch-size: 10
- 0 operators
- ran for 17 minutes


FA2-SmartPy has a build option to completely disable operators; but in
that case the permissions-descriptor entrypoint becomes mandatory.

We see a very slight gain in gas-usage when removing the operators,
but in the total amount used it it almost invisible.

EOF
    fig _out/b6/lifetime-usage.png
    fig _out/b6/transfer-gas-usage.png
    # b6 ran a bit less than 17h
    dune exec ./main.exe transfers results-b6_noops_vs_ops.tsv _out/b6/

    cat >> _out/results.md <<'EOF'
### Minting “NFTs”

- flavors: `mutran_contract` Vs `tocset_contract` Vs `lzep2_mutran_contract`
- batch-size: 1
- 0 operators

We ask the benchmarks to mint `n` tokens, and then (try to) perform
one transfer.

The new build is `tocset_contract` which keeps a Michelson `set` of
all known token-IDs (it removes the assumption that token-ids are
consecutive); it is a bit useless in the real-world but it is and
interesting benchmark.

Ran like this:

     ./please.sh run run-bench \
                mutran_contract tokset_contract lzep2_mutran_contract \
                --batch-sizes 1 --total-transfers 1 \
                --batching-style factor \
                --tokens $(seq -s , 1 1000) --ope 0 \
                --root $rp

⇒ We see that the gas usage increases when we use a `(set nat)` in storage:

EOF
    #dune exec ./main.exe mints results-b7-minting-a-lot-of-nfts.tsv _out/b8
    dune exec ./main.exe mints results-many-mints-b8.tsv _out/b8
    fig _out/b8/lifetime-usage.png
    fig _out/b8/mint-gas-usage.png

    cat >> _out/results.md <<'EOF'

We now try the same experiment with even more NFTs and we also add the
`dbg_contract`

This ran for 11 hours:

     fatoo run-bench contract tokset_contract dbg_contract lzep2_contract \
           --operators 0 \
           --batch-sizes 1 \
           --batching-style factor \
           --total 1 \
           --tokens $(seq -s, 1 10000) \
           --root /work/_bA_mint-many_root/$run

We see that `dbg_contract` runs out gas quickly, followed by `tokset_contract`.

While the normal builds are still in cruise mode.

EOF
    dune exec ./main.exe mints results-bA-many-mints.tsv _out/bA
    #fig _out/bA/lifetime-usage.png
    fig _out/bA/mint-gas-usage.png
    printf "\n\nLast successful mint for \`dbg_contract\`:\n\`\`\`\n" >> _out/results.md
    grep dbg_contract results-bA-many-mints.tsv  | tail -n 1 >> _out/results.md
    printf "\`\`\`\\n\nLast successful mint for \`tokset_contract\`:\n\`\`\`\n" >> _out/results.md
    grep tokset_contract results-bA-many-mints.tsv  | tail -n 1 >> _out/results.md
    printf "\`\`\`\\n\n⇒ Interesting limit for a set of natural numbers (the smallest natural numbers we can fit in there actually).\n\n" >> _out/results.md

    cat >> _out/results.md <<'EOF'
### Pushing the Natural Limits

a.k.a. limits of `nat`s

- flavors: we use only `mutran_contract`
- batch-size: 1
- 0 operators

This benchmark mints a few tokens but sets the balance to very large numbers:
`2 ^ (token-id * multiplier)`, where multiplier is 100 and then 10 000.

Fails around `2^120 000` because the resulting number literal hits the maximal
operation size.

Indeed binary serialization of a `Z.t` (Tezos' internal arbitrary
precision integer) uses 7 bits over every byte: `120 000 / 7 = 17143`
bytes (maximal operation size is around 16 kB).

Gas usage for 1 transfer is when current balace is `2^110 000`:
879 195 gas-units.

As fungibility of tokens goes: the estimated number of particles in
the universe is _only_ `2^286`.

EOF
    dune exec ./main.exe bignats results-special-mint-100x10000.tsv _out/b9.1/ 10000
    dune exec ./main.exe bignats results-special-mint-500x100.tsv _out/b9.2/ 100
    fig _out/b9.2/mint-gas-usage-vs-amount-100.png
    #fig _out/b9.2/lifetime-usage.png
    fig _out/b9.1/mint-gas-usage-vs-amount-10000.png
    #fig _out/b9.2/lifetime-usage.png
    # results-special-mint-100x1000.tsv
    # results-special-mint-100x10000.tsv
    # results-special-mint-500x100.tsv
    #find _out -name '*.png' -exec echo {} '→ ![](' "$PWD/"{} ")" \; >> _out/results.md
    pandoc -s --self-contained -i _out/results.md -o _out/results.html
    find _out -name '*.png'
    echo "Done: file://$PWD/_out/results.html"
}


{
    if [ "$1" = "" ] ; then
        make_report
    else
        "$@"
    fi
}





